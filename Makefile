# Environment 
CC=g++
FLAGS=-std=c++11 -w -O3
OUT=test
RM=rm -fr
TEMP=./temp/
#-----------------------------------------------------------------

all: main.o
	$(CC) main.o -o $(OUT)


# UNIVERSAL RULE
%.o : %.cpp
	$(CC) $(FLAGS) -c $< -o $@
	
# DEPENDENS GENERATOR
dep:
	$(CC) $(FLAGS) -MM *.cpp >dep.list
-include dep.list

# TESTS
test: all
#	python ./tests/test.py

test-start: FLAGS=-std=c++11 -O3 -Wall -Wextra -Werror -Wno-unused-value -pedantic -pedantic-errors -g
test-start: all
